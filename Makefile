#
# OpenCL for LuaJIT.
# Copyright © 2013 Peter Colberg.
# For conditions of distribution and use, see copyright notice in LICENSE.
#

opencl:
	@$(MAKE) -C opencl

clean:
	@$(MAKE) -C opencl clean
	@$(MAKE) -C doc clean

.PHONY: opencl clean
